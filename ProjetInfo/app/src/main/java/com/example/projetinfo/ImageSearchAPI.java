package com.example.projetinfo;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;

import com.example.projetinfo.model.ImagePost;
import com.example.projetinfo.model.ImageSearch;

import java.io.File;
import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ImageSearchAPI {

    String BASE_URL = "http://10.0.2.2:8000/";

    @GET("img_searches/results/{id}")
    Call<ImageSearch> getImages(@Path("id") int id);

    @POST("img_searches/upload/")
    Call<ImagePost> uploadFile(@Body File image);

    @Multipart
    @POST("img_searches/upload/")
    Call<ResponseBody> uploadImage(
        @Part MultipartBody.Part image
    );
}
