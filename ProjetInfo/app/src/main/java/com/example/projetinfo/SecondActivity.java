package com.example.projetinfo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.projetinfo.model.Result;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class SecondActivity extends AppCompatActivity {
    private String SERVER_ADDRESS = "http://10.0.2.2:8000";
    private static Context context;
    Result[] resultList = null;
    Button goBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.activity_2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        ArrayList<Result> results = (ArrayList<Result>) args.getSerializable("ARRAYLIST");

        for (final Result res : results) {
            Log.i("i", "similarity :" + res.getTaux());

            Double temp = Double.valueOf(res.getTaux()) * 100;
            DecimalFormat numberFormat = new DecimalFormat("#.##");
            res.setTaux(Double.valueOf(numberFormat.format(temp)));
            res.setImage(SERVER_ADDRESS + res.getImage());
        }

        resultList = results.toArray(new Result[0]);

        ListView listView = (ListView)findViewById(R.id.listView);
        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);
    }

    public static Bitmap LoadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            Bitmap image = ((BitmapDrawable)d).getBitmap();
            return image;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return resultList.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.customlayout,null);

            ImageView imageview = (ImageView) view.findViewById(R.id.imageView);
            TextView textview = (TextView) view.findViewById(R.id.textView);

            Picasso.with(context).load(resultList[position].getImage()).into(imageview);
            Drawable tempImg = imageview.getDrawable();
            String temp = resultList[position].getImage();
            textview.setText("Similarité = " + (resultList[position].getTaux()) +"%");
            return view;
        }
    }
}
