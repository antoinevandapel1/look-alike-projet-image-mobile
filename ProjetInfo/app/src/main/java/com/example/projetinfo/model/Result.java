package com.example.projetinfo.model;

import android.graphics.Bitmap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.File;

public class Result implements java.io.Serializable{

    @SerializedName("taux")
    @Expose
    private Double taux;
    @SerializedName("image")
    @Expose
    private String image;
    private Bitmap convertedImage;

    public Double getTaux() {
        return taux;
    }

    public void setTaux(Double taux) {
        this.taux = taux;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Bitmap getConvertedImage() {
        return convertedImage;
    }

    public void setConvertedImage(Bitmap convertedImage) {
        this.convertedImage = convertedImage;
    }
}
