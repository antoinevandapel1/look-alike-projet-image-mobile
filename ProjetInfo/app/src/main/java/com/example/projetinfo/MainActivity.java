package com.example.projetinfo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.projetinfo.model.ImageSearch;
import com.example.projetinfo.model.Result;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    // static
    private static final int RETURN_PHOTO_TAKEN = 1;
    private static final int RETURN_PHOTO_GALLERY = 2;
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    // properties
    private Button buttonPhoto;
    private Button buttonGallery;
    private Button buttonSearch;
    private ImageView imageView;
    private String photoPath = null;
    private Uri selectedImageUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        verifyStoragePermissions(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imgPhoto);
        buttonPhoto = findViewById(R.id.btnPhoto);
        buttonGallery = findViewById(R.id.btnGallery);
        buttonSearch = findViewById(R.id.btnSearch);

        if (savedInstanceState != null){
            selectedImageUri = savedInstanceState.getParcelable("Uri");
            if (selectedImageUri != null){
                imageView.setImageURI(selectedImageUri);
            }
            photoPath = savedInstanceState.getString("PhotoPath");
        }

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ImageSearchAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        buttonPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // intent.setType("image/#");

                if(intent.resolveActivity(getPackageManager()) != null){
                    String time = new SimpleDateFormat("yyyyMMdd_HHmmSS").format(new Date());
                    File photoDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

                    try {
                        File photoFile = File.createTempFile(time, ".jpg", photoDir);
                        photoPath = photoFile.getAbsolutePath();
                        Uri photoUri = FileProvider.getUriForFile(MainActivity.this,
                                MainActivity.this.getApplicationContext().getPackageName()+".provider", photoFile);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

                        startActivityForResult(intent, RETURN_PHOTO_TAKEN);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

        buttonGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/#");

                startActivityForResult(Intent.createChooser(intent, "Choisissez une image"), RETURN_PHOTO_GALLERY);
            }
        });

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imageView.getDrawable() != null){
                    Log.i("i", "call GO");
                    ImageSearchAPI api = retrofit.create(ImageSearchAPI.class);

                    createPost();
                }
                else {
                    Toast.makeText(MainActivity.this, "Aucune image sélectionnée", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        //Check requestCode and state of return
        if (requestCode==RETURN_PHOTO_TAKEN && resultCode==RESULT_OK){
            Log.i("i", "photoPath : " + photoPath);
            Bitmap image = BitmapFactory.decodeFile(photoPath);
            imageView.setImageBitmap(image);
            File temp = new File(photoPath);
            selectedImageUri = Uri.fromFile(temp);
        }
        else if (requestCode==RETURN_PHOTO_GALLERY && resultCode==RESULT_OK) {
            selectedImageUri = data.getData();


            File tempFile = new File(selectedImageUri.getPath());

            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImageUri, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
            photoPath = imagePath;

            Log.i("i", "photoPath : " + imagePath);
            if (selectedImageUri != null){

                imageView.setImageURI(selectedImageUri);
            }
        }
    }

    private void createPost() {
        File imageFile = new File(photoPath);

        Log.i("i", "PhotoPath avant envoi : " + photoPath);

        Uri test = Uri.parse(photoPath);
        RequestBody imagePart = RequestBody.create(
                MediaType.parse(photoPath),
                imageFile);

        MultipartBody.Part image = MultipartBody.Part.createFormData("image", imageFile.getName(), imagePart);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ImageSearchAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ImageSearchAPI api = retrofit.create(ImageSearchAPI.class);

        Call<ResponseBody> call = api.uploadImage(image);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()){
                    Log.e("e", "Code : " + response.code());
                    Log.e("e", "Body :" + response.errorBody());
                    return;
                }

//                ImagePost imgResponse = response.body();
                try {
                    String content = response.body().string();
                    JSONObject obj = new JSONObject(content);

                    Log.i("i", "test : " + obj.getInt(("id")));
                    getSimilarImages(obj.getInt(("id")));
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
//                content += "ID :" + imgResponse.getId() + "\n";
//                content += "image :" + imgResponse.getImage().getPath() + "\n";
//                content += "date :" + imgResponse.getSearchDate().toString() + "\n\n";

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("e", "Erreur POST : " + t.getMessage());
            }
        });;
    }

    private void getSimilarImages(int id){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ImageSearchAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ImageSearchAPI api = retrofit.create(ImageSearchAPI.class);
        Call<ImageSearch> call = api.getImages(id);
        call.enqueue(new Callback<ImageSearch>() {
            @Override
            public void onResponse(Call<ImageSearch> call, Response<ImageSearch> response) {
                Log.i("i", "GET OK");

                ImageSearch result = response.body();

//                for (Result res : result.getResults()) {
//                    Log.i("i", "similarity :" + res.getSimilarity());
//                }

                openSecondActivity(result.getResults());
            }

            @Override
            public void onFailure(Call<ImageSearch> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable("Uri", selectedImageUri);
        outState.putString("PhotoPath", photoPath);
    }

    public void openSecondActivity(List<Result> resultsToDisplay){
        Intent intent = new Intent(this, SecondActivity.class);
        Bundle args = new Bundle();
        args.putSerializable("ARRAYLIST", (Serializable)resultsToDisplay);
        intent.putExtra("BUNDLE",args);
        startActivity(intent);
    }

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

}
