package com.example.projetinfo.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.Date;

public class ImagePost {

    @Expose
    @SerializedName("id")
    private Integer id;
    @Expose
    @SerializedName("image")
    private File image;
    @Expose
    @SerializedName("search_date")
    private String searchDate;


    public Integer getId() {
        return id;
    }

    public File getImage() {
        return image;
    }

    public String getSearchDate() {
        return searchDate;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setImage(File image) {
        this.image = image;
    }

    public void setSearchDate(String searchDate) {
        this.searchDate = searchDate;
    }
}
