from django.conf.global_settings import MEDIA_ROOT
from rest_framework import serializers

from .models import Recherche, Resultat


class RechercheSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recherche
        fields = ['id', 'image', 'search_date']


class ResultatSerializer(serializers.ModelSerializer):

    image = serializers.SerializerMethodField("get_image")

    class Meta:
        model = Resultat
        fields = ["taux", "image"]

    def get_image(self, instance):
        return instance.image.url
